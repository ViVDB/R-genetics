# R-genetics
A part of the practical sessions for the Genetics &amp; Bioinformatics Course at ULg (ULiege).
___

This guide is incomplete (I've never had the time to finish it and now passed that course) and not proof-read but aims to provide a comprehensible explanation of the different parameters and concepts used in the first and second TP session of the course GBIO0002 (in 2018). Especially since bachelor students haven't seen some key statistical concepts yet, those concepts are roughly explained.

For the statistical explanation, the book "Statistics in plain english", which is referenced in full in the pdf, has been the most useful.

## Copyright

Copyright 2018 ViVDB